package com.hrd.spring_hw2.repository;

import com.hrd.spring_hw2.model.Customer;
import com.hrd.spring_hw2.model.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface CustomerRepository {

    @Select("""
            SELECT * FROM CUSTOMER_TABLE
            """)
    @Results(
            id = "customerMap", value = {
               @Result(property = "customerId", column = "customer_id"),
               @Result(property = "customerName", column = "customer_name"),
               @Result(property = "customerAddress", column = "customer_address"),
               @Result(property = "customerPhone", column = "customer_phone"),
    }
    )

    List<Customer> getAll();


    @Select("""
            select * from customer_table where customer_id = #{id}
            """)
    @ResultMap("customerMap")
    Customer getByID(int id);


    @Select("""
            insert into customer_table ( customer_name, customer_address, customer_phone)
            values(#{cs.customerName}, #{cs.customerAddress}, #{cs.customerPhone}) returning *
            """)
    @ResultMap("customerMap")
    Customer insertData(@Param("cs") Customer customer);

    @Select("""
            DELETE FROM customer_table WHERE customer_id = #{id} returning *
            """)
    @ResultMap("customerMap")
    Customer deleteCustomerById(Integer id);


    @Select("""
           update customer_table set customer_name = #{cus.customerName},customer_address= #{cus.customerAddress}, customer_phone = #{cus.customerPhone} where customer_id = #{id}
            """)
    @ResultMap("customerMap")
    Customer updateCustomerByID(Integer id, @Param("cus") Customer customer);
}
