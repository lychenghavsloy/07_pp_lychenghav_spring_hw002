package com.hrd.spring_hw2.repository;

import com.hrd.spring_hw2.model.Invoice;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface InvoiceRepository {


    @Select("SELECT * FROM invoice_table")
    @Results(id = "InvoiceMap", value = {
            @Result(property = "invoiceId", column = "invoice_id"),
            @Result(property = "invoiceDate", column = "invoice_date"),
            @Result(property = "customer", column = "customer_id", one = @One(select = "com.hrd.spring_hw2.repository.CustomerRepository.getByID")),
            @Result(property = "productList", column = "invoice_id", many = @Many(select = "com.hrd.spring_hw2.repository.ProductRepository.getProductsById"))
    })
    List<Invoice> getAllInvoice();

    @Select("SELECT * FROM invoice_table where invoice_id = #{id}")
    @ResultMap("InvoiceMap")
    Invoice getInvoiceById(Integer id);

    @Select("DELETE FROM invoice_table where invoice_id = #{id}")
    @ResultMap("InvoiceMap")
    Invoice deleteInvoiceById(Integer id);

}
