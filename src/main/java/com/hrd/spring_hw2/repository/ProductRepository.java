package com.hrd.spring_hw2.repository;

import com.hrd.spring_hw2.model.Product;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
@Repository
public interface ProductRepository {

    @Select("""
            SELECT * FROM product_table 
            """)
    @Results(id = "productMap", value = {
            @Result(property = "productId", column = "product_id"),
            @Result(property = "productName", column = "product_name"),
            @Result(property = "productPrice", column = "product_price")
    }
    )

    List<Product> getAllProduct();

    @Select("""
            Select * from product_table where product_id = #{id};
            """)
    @ResultMap("productMap")
    Product getProductById(Integer id);

    @Select("""
            insert into product_table (product_name, product_price) 
            values(#{pd.productName}, #{pd.productPrice}) returning *
            """)
    @ResultMap("productMap")
    Product insertData(@Param("pd") Product product);


    @Select("""
            delete from product_table where product_id = #{id} returning *
            """)
    @ResultMap("productMap")
    Product deleteproductById(int id);

    @Select("""
           update product_table set product_name = #{product.productName}, product_price = #{product.productPrice} where product_id = #{id} returning *
            """)
    @ResultMap("productMap")
    Product updateByID(int id, @Param("product") Product product);

    @Select("SELECT p.product_id, product_name, product_price FROM product_table p JOIN invoice_detail_table i on p.product_id = i.product_id where i.invoice_id = #{id}")
    @ResultMap("productMap")
    List<Product> getProductsById(Integer id);
}
