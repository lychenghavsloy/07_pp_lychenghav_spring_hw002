package com.hrd.spring_hw2.controller;

import com.hrd.spring_hw2.model.Customer;
import com.hrd.spring_hw2.model.Product;
import com.hrd.spring_hw2.model.entity;
import com.hrd.spring_hw2.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Product_RestController {

    private final ProductService productService;

    public Product_RestController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/api/v1/get-all/product")
    public ResponseEntity<?> getAllProduct() {
        return ResponseEntity.ok(new entity<List<Product>>(
                productService.getAllProduct(),
                "String",
                "True"
        ));
    }

    @GetMapping("/api/v1/get-product-by-id/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Integer id) {
        Product product = productService.getProductById(id);
        entity<Product> res = new entity<>(
                product,
                "String",
                "True"
        );
        return ResponseEntity.ok(res);
    }

    @PostMapping("/api/v1/add-new-product")
    public ResponseEntity<?> newProduct(@RequestBody Product product) {
        entity entity = new entity<Product>();
        entity.setMessage("Insert to Database Successfully");
        entity.setPayload(productService.insertData(product));
        entity.setSuccess(true);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/api/v1/update-product-by-id/{id}")
    public ResponseEntity<?> updateById(@RequestBody Product product ,@PathVariable Integer id) {
        entity entity = new entity<Product>();
        entity.setMessage("Update Successfully");
        productService.updateByID(id,product);
        entity.setPayload(productService.getProductById(id));
        return ResponseEntity.ok().body(entity);
    }

    @DeleteMapping("/api/v1/delete-product-by-id/{id}")
    public ResponseEntity<?> deleteProductById(@PathVariable Integer id) {
        Product product = productService.deleteProductById(id);
        entity res = new entity<Product>(
                product,
                "String",
                "True"
        );
        return ResponseEntity.ok(res);
    }
}