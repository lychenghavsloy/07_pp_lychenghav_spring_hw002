package com.hrd.spring_hw2.controller;

import com.hrd.spring_hw2.model.Invoice;
import com.hrd.spring_hw2.model.entity;
import com.hrd.spring_hw2.service.InvoiceService;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Invoice_RestController {

    private final InvoiceService invoiceService;

    public Invoice_RestController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    @GetMapping("/api/v1/get-all/invoice")
    public ResponseEntity<?> getAllInvoice(){
        return ResponseEntity.ok(new entity<List<Invoice>>(
                invoiceService.getAllInvoice(),
                "string",
                "True"
        ));
        }


    @GetMapping("/api/v1/get-invoice-by-id/{id}")
    public ResponseEntity<?> getInvoiceById (@PathVariable Integer id){
        Invoice invoice = invoiceService.getInvoiceById(id);
            entity<Invoice> res = new entity<>(
                    invoice,
                    "String",
                    "True"
            );
            return ResponseEntity.ok(res);
    }

    @PostMapping("/api/v1/add-new-invoice")
    public ResponseEntity<?> newInvoice (@RequestBody Invoice invoice){
        entity entity = new entity<Invoice>();
        entity.setMessage("Insert Successfully");
        entity.setPayload(invoiceService.insertNewInvoice(invoice));
        entity.setSuccess(true);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/api/v1/update-invoice-by-id/{id}")
    public ResponseEntity<?> updateInvoiceById (){
        return updateInvoiceById();
    }

    @DeleteMapping("/api/v1/delete-invoice-by-id/{id}")
    public ResponseEntity<?> deleteInvoiceById (@PathVariable Integer id){
        Invoice invoice = invoiceService.deleteInvoiceById(id);
        entity res = new entity<Invoice>(
                null,
                "Deleted Successfully",
                "True"
        );
        return ResponseEntity.ok(res);
    }
}
