package com.hrd.spring_hw2.controller;

import com.hrd.spring_hw2.model.Customer;
import com.hrd.spring_hw2.model.Product;
import com.hrd.spring_hw2.model.entity;
import com.hrd.spring_hw2.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Customer_RestController {
    private final CustomerService customerService;

    public Customer_RestController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/api/v1/get-all/customer")
    public ResponseEntity<?> getAllCustomer (){
        entity entity = new entity<List<Customer>>();
        entity.setMessage("Fetched All Data Successfully");
        entity.setPayload(customerService.getAll());
        entity.setSuccess(true);
        return ResponseEntity.ok().body(entity);
    }

    @GetMapping("/api/v1/get/id/{id}")
    public ResponseEntity<?> getCustomerByID (@PathVariable int id){
        entity entity = new entity<Customer>();
        entity.setMessage("Fetched All Data Successfully");
        entity.setPayload(customerService.getByID(id));
        entity.setSuccess(true);
        if(customerService.getByID(id) == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(entity);
    }

    @PostMapping("/api/v1/add-new-customer")
    public ResponseEntity<?> newCustomer(@RequestBody Customer customer) {
        entity entity = new entity<Customer>();
        entity.setMessage("Insert to Database Successfully");
        entity.setPayload(customerService.insertData(customer));
        entity.setSuccess(true);
        return ResponseEntity.ok().body(entity);
    }

    @PutMapping("/api/v1/update-customer-by-id/{id}")
    public ResponseEntity<?> updateCustomerById(@RequestBody Customer customer ,@PathVariable Integer id) {
        entity entity = new entity<Customer>();
        entity.setMessage("Update Successfully");
        customerService.updateCustomerByID(id,customer);
        entity.setPayload(customerService.getByID(id));
        return ResponseEntity.ok().body(entity);
    }

    @DeleteMapping("/api/v1/delete-customer-by-id/{id}")
    public ResponseEntity<?> deleteCustomerById(@PathVariable Integer id) {
        Customer customer = customerService.deleteCustomerById(id);
        entity res = new entity<Customer>(
                customer,
                "String",
                "True"
        );
        return ResponseEntity.ok(res);
    }
}
