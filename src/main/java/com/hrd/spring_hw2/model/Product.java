package com.hrd.spring_hw2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    @JsonProperty (access = JsonProperty.Access.READ_ONLY)
    private Integer productId;
    private String productName;

    private Double productPrice;
}
