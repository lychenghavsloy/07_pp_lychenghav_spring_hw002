package com.hrd.spring_hw2.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class entity<T> {
@JsonInclude(JsonInclude.Include.NON_NULL)
private T payload;
private String message;
private boolean success = true;

    public entity(T payload, String message, String success) {
        this.payload = payload;
        this.message = message;
        this.success = Boolean.parseBoolean(success);
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
