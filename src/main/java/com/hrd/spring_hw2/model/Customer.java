package com.hrd.spring_hw2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Customer {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private Integer customerPhone;
}
