package com.hrd.spring_hw2.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Invoice {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)

    private Integer invoiceId;
    private LocalDateTime invoiceDate;
    private Customer customer;
    private List<Product>productList;
}
