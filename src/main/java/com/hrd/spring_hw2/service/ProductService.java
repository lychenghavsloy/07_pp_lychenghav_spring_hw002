package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product getProductById(Integer id);

    Product insertData(Product product);

    Product deleteProductById(int id);


    Product updateByID(int id, Product product);
}
