package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Customer;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CustomerService {

    List<Customer> getAll();

    Customer getByID(int id);


    Customer insertData(Customer customer);


    Customer deleteCustomerById(Integer id);

    Customer updateCustomerByID(Integer id, Customer customer);

    Customer getCustomerById(Integer id);
}
