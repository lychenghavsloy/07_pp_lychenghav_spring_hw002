package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Customer;
import com.hrd.spring_hw2.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService{

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAll() {
        return customerRepository.getAll();
    }

    @Override
    public Customer getByID(int id) {
        return customerRepository.getByID(id);
    }

    @Override
    public Customer insertData(Customer customer) {
        return customerRepository.insertData(customer);
    }

    @Override
    public Customer deleteCustomerById(Integer id) {
        return customerRepository.deleteCustomerById(id);
    }

    @Override
    public Customer updateCustomerByID(Integer id, Customer customer) {
        return customerRepository.updateCustomerByID(id,customer);
    }

    @Override
    public Customer getCustomerById(Integer id) {
        return null;
    }
}
