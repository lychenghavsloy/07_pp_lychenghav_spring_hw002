package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Invoice;
import com.hrd.spring_hw2.repository.InvoiceRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

@Component
@Repository
public class InvoiceServiceImpl implements InvoiceService{

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Invoice getInvoiceById(Integer id) {
        return invoiceRepository.getInvoiceById(id);
    }

    @Override
    public Invoice deleteInvoiceById(Integer id) {
        return invoiceRepository.deleteInvoiceById(id);
    }

    @Override
    public Invoice insertNewInvoice(Invoice invoice) {
        return null;
    }

}
