package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Product;
import com.hrd.spring_hw2.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService{

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public Product getProductById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public Product insertData(Product product) {
        return productRepository.insertData(product);
    }

    @Override
    public Product deleteProductById(int id) {
        return productRepository.deleteproductById( id);
    }

    @Override
    public Product updateByID(int id, Product product) {
        return productRepository.updateByID(id, product);
    }



}
