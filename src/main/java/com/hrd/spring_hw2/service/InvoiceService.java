package com.hrd.spring_hw2.service;

import com.hrd.spring_hw2.model.Invoice;

import java.util.List;

public interface InvoiceService {
    List<Invoice> getAllInvoice();

    Invoice getInvoiceById(Integer id);

    Invoice deleteInvoiceById(Integer id);

    Invoice insertNewInvoice(Invoice invoice);
}
